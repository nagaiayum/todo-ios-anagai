//
//  TodoGetRequestTests.swift
//  TodoappTests
//
//  Created by anagai on 2020/09/01.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import Todoapp

final class TodosGetRequestTests: XCTestCase {
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodosGetRequest()
        XCTAssertEqual(request.method, .get)
        XCTAssertEqual(request.path, "/todos")
    }

    func testResponse() {
        var todosGetResponse: TodosGetResponse?

        stub(condition: isHost("todo-server-anagai.herokuapp.com")) { _ in
            HTTPStubsResponse(
                fileAtPath: OHPathForFile("TodosGetResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let exp = expectation(description: "Get Todo Request")
        
        ApiClient().call(
            request: TodosGetRequest(),
            success: { response in
                todosGetResponse = response
                exp.fulfill()
        },
            failure: { _ in
        })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todosGetResponse)
            XCTAssertEqual(todosGetResponse?.errorCode, 0)
            XCTAssertEqual(todosGetResponse?.errorMessage, "")
            XCTAssertEqual(todosGetResponse?.todos.count, 3)
            XCTAssertEqual(todosGetResponse?.todos[0].title, "NAGAI")
            XCTAssertEqual(todosGetResponse?.todos[0].detail, "MYBIRTHDAY")
            XCTAssertEqual(todosGetResponse?.todos[0].date?.toString(), "2000-03-09T00:00:00.000Z")
            XCTAssertEqual(todosGetResponse?.todos[1].title, "nagai")
            XCTAssertEqual(todosGetResponse?.todos[1].detail, "test")
            XCTAssertNil(todosGetResponse?.todos[1].date)
            XCTAssertEqual(todosGetResponse?.todos[2].title, "ayumu")
            XCTAssertNil(todosGetResponse?.todos[2].detail)
            XCTAssertNil(todosGetResponse?.todos[2].date)
        }
    }
}

extension Date {
    func toString() -> String? {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "JST")
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return formatter.string(from: self)
    }
}
