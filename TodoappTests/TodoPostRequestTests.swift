//
//  TodoPostRequestTests.swift
//  TodoappTests
//
//  Created by anagai on 2020/09/01.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import Todoapp

final class TodoPostRequestTests: XCTestCase {
    let postRequest = TodoPostRequest(title: "ABCDEFG", detail: nil, date: nil)

    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = postRequest
        XCTAssertEqual(request.method, .post)
        XCTAssertEqual(request.path, "/todos")
    }

    func testResponse() {
        var todosPostReponse: CommonResponse?

        stub(condition: isHost("todo-server-anagai.herokuapp.com")) { _ in
            HTTPStubsResponse(
                fileAtPath: OHPathForFile("SuccessResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let exp = expectation(description: "Create Todo Request")
        
        ApiClient().call(
            request: postRequest,
            success: { response in
                todosPostReponse = response
                exp.fulfill()
        },
            failure: { _ in
        })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todosPostReponse)
            XCTAssertEqual(todosPostReponse?.errorCode, 0)
            XCTAssertEqual(todosPostReponse?.errorMessage, "")
        }
    }
}
