//
//  TodoUpdateRequestTests.swift
//  TodoappTests
//
//  Created by anagai on 2020/09/01.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import Todoapp

final class TodoUpdateRequestTests: XCTestCase {
    private let todo = Todo(id: 1, title: "ayumu", detail: "nagai", date: nil)
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodoUpdateRequest(todo: todo)
        XCTAssertEqual(request.method, .put)
        XCTAssertEqual(request.path, "/todos/1")
    }

    func testResponse() {
        var todoUpdateResponse: CommonResponse?

        stub(condition: isHost("todo-server-anagai.herokuapp.com")) { _ in
            HTTPStubsResponse(
                fileAtPath: OHPathForFile("SuccessResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let exp = expectation(description: "Update Todo Request")
        
        ApiClient().call(
            request: TodoUpdateRequest(todo: todo),
            success: { response in
                todoUpdateResponse = response
                exp.fulfill()
        },
            failure: { _ in
        })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoUpdateResponse)
            XCTAssertEqual(todoUpdateResponse?.errorCode, 0)
            XCTAssertEqual(todoUpdateResponse?.errorMessage, "")
        }
    }
}
