//
//  TodoDeleteRequestTests.swift
//  TodoappTests
//
//  Created by anagai on 2020/07/28.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import Todoapp

final class TodoDeleteRequestTests: XCTestCase {
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodoDeleteRequest(id: 1)
        XCTAssertEqual(request.method, .delete)
        XCTAssertEqual(request.path, "/todos/1")
    }

    func testResponse() {
        var todoDeleteResponse: CommonResponse?

        stub(condition: isHost("todo-server-anagai.herokuapp.com")) { _ in
            HTTPStubsResponse(
                fileAtPath: OHPathForFile("SuccessResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let exp = expectation(description: "Delete Todo Request")
        
        ApiClient().call(
            request: TodoDeleteRequest(id: 1),
            success: { response in
                todoDeleteResponse = response
                exp.fulfill()
        },
            failure: { _ in
        })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoDeleteResponse)
            XCTAssertEqual(todoDeleteResponse?.errorCode, 0)
            XCTAssertEqual(todoDeleteResponse?.errorMessage, "")
        }
    }
}
