//
//  TestRequestProtocol.swift
//  TodoappTests
//
//  Created by anagai on 2020/09/01.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import Alamofire
@testable import Todoapp

final class TestRequestProtocol: RequestProtocol {
    typealias Response = TodosGetResponse

    var path: String {
        "/todos"
    }

    var method: HTTPMethod {
        .get
    }

    var parameters: Parameters? {
        nil
    }

    var baseUrl: String {
        "https://todo-server-anagai.herokuapp.com/"
    }

    var encoding: ParameterEncoding {
        JSONEncoding.default
    }

    var headers: HTTPHeaders? {
        ["Content-Type": "application/json"]
    }
}

extension ParameterEncoding {
    func toJsonEncoding() -> JSONEncoding? {
        self as? JSONEncoding
    }
}

extension JSONEncoding: Equatable {
    public static func == (lhs: JSONEncoding, rhs: JSONEncoding) -> Bool {
        lhs.options == rhs.options
    }
}
