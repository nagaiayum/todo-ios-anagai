//
//  RequestProtocolTests.swift
//  TodoappTests
//
//  Created by anagai on 2020/09/01.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import XCTest

final class RequestProtocolTests: XCTestCase {
    func testInit() {
        let request = TestRequestProtocol()

        XCTAssertNil(request.parameters)
        XCTAssertEqual(request.baseUrl, "https://todo-server-anagai.herokuapp.com/")
        XCTAssertEqual(request.headers?["Content-Type"], "application/json")
        XCTAssertEqual(request.encoding.toJsonEncoding(), .default)
        XCTAssertEqual(request.method, .get)
        XCTAssertEqual(request.path, "/todos")
    }
}
