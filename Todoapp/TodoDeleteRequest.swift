//
//  TodoDeleteRequest.swift
//  Todoapp
//
//  Created by anagai on 2020/07/27.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import Alamofire

struct TodoDeleteRequest: RequestProtocol {
    typealias Response = CommonResponse

    let id: Int

    var parameters: Parameters? {
        nil
    }
    var path: String {
        "/todos/\(id)"
    }
    var method: HTTPMethod {
        .delete
    }
}
