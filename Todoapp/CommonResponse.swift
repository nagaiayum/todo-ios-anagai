//
//  CommonResponse.swift
//  Todoapp
//
//  Created by anagai on 2020/06/29.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import Foundation

struct CommonResponse: BaseResponse {
    var errorCode: Int
    var errorMessage: String
}
