//
//  TodosGetRequest.swift
//  Todoapp
//
//  Created by anagai on 2020/06/25.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import Alamofire

struct TodosGetRequest: RequestProtocol {
    typealias Response = TodosGetResponse

    var parameters: Parameters? {
        nil
    }
    var path: String {
        "/todos"
    }
    var method: HTTPMethod {
        .get
    }
}
