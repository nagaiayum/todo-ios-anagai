//
//  BaseResponse.swift
//  Todoapp
//
//  Created by anagai on 2020/06/24.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import Foundation

protocol BaseResponse: Codable {
    var errorCode: Int { get }
    var errorMessage: String { get }
}
