//
//  RequestProtocol.swift
//  Todoapp
//
//  Created by anagai on 2020/06/24.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import Alamofire

protocol RequestProtocol {
    associatedtype Response: Codable
    var baseUrl: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var encoding: ParameterEncoding { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get }
}

// 共通するものはデフォルト値設定
extension RequestProtocol {
    var baseUrl: String {
        "https://todo-server-anagai.herokuapp.com/"
    }
    var encoding: ParameterEncoding {
        JSONEncoding.default
    }
    var headers: HTTPHeaders? {
        ["Content-Type": "application/json"]
    }
}
