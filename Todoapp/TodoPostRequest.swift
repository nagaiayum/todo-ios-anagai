//
//  TodoPostRequest.swift
//  Todoapp
//
//  Created by anagai on 2020/07/06.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import Alamofire

struct TodoPostRequest: RequestProtocol {
    typealias Response = CommonResponse

    let title: String
    let detail: String?
    let date: Date?

    var path: String {
        "/todos"
    }
    var method: HTTPMethod {
        .post
    }

    var parameters: Parameters? {
        var parameters = ["title": title]
        if let detail = detail {
            parameters["detail"] = detail
        }
        if let date = date {
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(identifier: "JST")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            parameters["date"] = formatter.string(from: date)
        }
        return parameters
    }
}
