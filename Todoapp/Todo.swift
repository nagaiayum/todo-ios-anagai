//
//  Todo.swift
//  Todoapp
//
//  Created by anagai on 2020/06/24.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import Foundation

struct Todo: Codable {
    let id: Int
    let title: String
    let detail: String?
    let date: Date?
}
