//
//  TodoEditViewController.swift
//  Todoapp
//
//  Created by anagai on 2020/06/15.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import UIKit

protocol TodoEditViewControllerDelegate: class {
    func todoEditViewControllerDidEditTodo(_ todoEditViewController: TodoEditViewController, message: String)
}

final class TodoEditViewController: UIViewController {
    weak var delegate: TodoEditViewControllerDelegate?
    @IBOutlet private weak var titleTextField: UITextField!
    @IBOutlet private weak var titleCountLabel: UILabel!
    @IBOutlet private weak var detailTextView: UITextView!
    @IBOutlet private weak var detailCountLabel: UILabel!
    @IBOutlet private weak var dateTextField: UITextField!
    @IBOutlet private weak var registerButton: UIButton!

    var todo: Todo?
    private let dateFormatter = DateFormatter()
    private let datePicker = UIDatePicker()
    private let maxTitleLength = 100
    private let maxDetailLength = 1000

    private var titleCount: Int {
        titleTextField.text?.count ?? 0
    }
    private var detailCount: Int {
        detailTextView.text?.count ?? 0
    }
    private var isTitleLimitOver: Bool {
        titleCount > maxTitleLength
    }
    private var isDetailLimitOver: Bool {
        detailCount > maxDetailLength
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy/M/d"
        navigationItem.title = "TODO APP"
        registerButton.isEnabled = false
        setUpDetailTextView()
        setUpDatePicker()
        titleTextField.addTarget(self, action: #selector(didChangeTitleText(_:)), for: .editingChanged)
        setUpRegisterButtonTitle()
        if let todo = todo {
            setUpText(todo: todo)
            setUpTextCount(todo: todo)
        }
        changeRegisterButtonState()
    }

    private func setUpDetailTextView() {
        detailTextView.layer.masksToBounds = true
        detailTextView.layer.borderWidth = 1
        detailTextView.layer.borderColor = UIColor.black.cgColor
        detailTextView.layer.cornerRadius = 10.0
        detailTextView.delegate = self
    }

    private func setUpDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.timeZone = NSTimeZone.local
        datePicker.locale = .current
        dateTextField.inputView = datePicker
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 40))
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                        target: nil,
                                        action: nil)
        let closeItem = UIBarButtonItem(title: "閉じる",
                                        style: .plain,
                                        target: self,
                                        action: #selector(didCloseButtonTapped))
        let doneItem = UIBarButtonItem(title: "決定",
                                       style: .done,
                                       target: self,
                                       action: #selector(didDoneButtonTapped))
        let deleteItem = UIBarButtonItem(title: "削除",
                                         style: .plain,
                                         target: self,
                                         action: #selector(didDeleteButtonTapped))
        toolbar.setItems([deleteItem, spaceItem, closeItem, doneItem], animated: true)
        dateTextField.inputAccessoryView = toolbar
    }

    private func setUpText(todo: Todo) {
        titleTextField.text = todo.title
        detailTextView.text = todo.detail
        if let date = todo.date {
            dateTextField.text = dateFormatter.string(from: date)
        }
    }

    private func setUpTextCount(todo: Todo) {
        titleCountLabel.text = String(todo.title.count)
        detailCountLabel.text = String(todo.detail?.count ?? 0)
    }
    
    private func registerTodo(todoPostRequest: TodoPostRequest) {
        ApiClient().call(
            request: todoPostRequest,
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidEditTodo(self, message: "登録しました")
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func updateTodo(todoUpdateRequest: TodoUpdateRequest) {
        ApiClient().call(
            request: todoUpdateRequest,
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidEditTodo(self, message: "更新しました")
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    @IBAction private func didTapRegistrationButton(_ sender: Any) {
        guard let title = titleTextField.text else { return }
        let detail = detailTextView.text.isEmpty ? nil : detailTextView.text
        let dateText = dateTextField.text ?? ""
        let date = dateFormatter.date(from: dateText)
        if let id = todo?.id {
            let todo = Todo(id: id, title: title, detail: detail, date: date)
            updateTodo(todoUpdateRequest: TodoUpdateRequest(todo: todo))
        } else {
            registerTodo(todoPostRequest: TodoPostRequest(title: title, detail: detail, date: date))
        }
    }

    @objc private func didChangeTitleText(_ sender: UITextField) {
        titleCountLabel.text = String(titleCount)
        changeTitleCounterColor()
        changeRegisterButtonState()
    }

    @objc private func didDoneButtonTapped() {
        dateTextField.endEditing(true)
        dateTextField.text = dateFormatter.string(from: datePicker.date)
    }

    @objc private func didCloseButtonTapped() {
        dateTextField.endEditing(true)
    }

    @objc private func didDeleteButtonTapped() {
        dateTextField.text = ""
        dateTextField.endEditing(true)
    }

    private func changeDetailCounterColor() {
        if isDetailLimitOver {
            detailCountLabel.textColor = .red
        } else {
            detailCountLabel.textColor = .black
        }
    }

    private func changeTitleCounterColor() {
        if isTitleLimitOver {
            titleCountLabel.textColor = .red
        } else {
            titleCountLabel.textColor = .black
        }
    }

    private func changeRegisterButtonState() {
        if titleCount == 0 || isTitleLimitOver || isDetailLimitOver {
            disableRegisterButton()
        } else {
            enableRegisterButton()
        }
    }

    private func setUpRegisterButtonTitle() {
        let title = todo == nil ? "登録" : "更新"
        registerButton.setTitle(title, for: .normal)
    }

    private func enableRegisterButton() {
        registerButton.backgroundColor = .blue
        registerButton.isEnabled = true
    }

    private func disableRegisterButton() {
        registerButton.backgroundColor = .gray
        registerButton.isEnabled = false
    }
}

extension TodoEditViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        detailCountLabel.text = String(detailCount)
        changeDetailCounterColor()
        changeRegisterButtonState()
    }
}
