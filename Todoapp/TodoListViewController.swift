//
//  TodoListViewController.swift
//  Todoapp
//
//  Created by anagai on 2020/06/09.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialSnackbar

final class TodoListViewController: UIViewController {
    @IBOutlet private weak var todoTableView: UITableView!
    private var isDeleteMode = false
    private var pencilBarButtonItem: UIBarButtonItem!
    private var todos: [Todo] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        todoTableView.dataSource = self
        todoTableView.delegate = self
        setUpNavigationItem()
        fetchTodoList()
    }

    private func fetchTodoList() {
        ApiClient().call(
            request: TodosGetRequest(),
            success: { [weak self] result in
                self?.todos = result.todos
                self?.todoTableView.reloadData()
            },
            failure: { [weak self] errorMessage in
                self?.showErrorAlert(message: errorMessage)
            }
        )
    }

    private func deleteTodo(id: Int) {
        ApiClient().call(
            request: TodoDeleteRequest(id: id),
            success: { [weak self] _ in
                self?.fetchTodoList()
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func setUpNavigationItem() {
        navigationItem.title = "TODO APP"
        let trashBarButtonItem = UIBarButtonItem(barButtonSystemItem: .trash,
                                                 target: self,
                                                 action: #selector(didTrashBarButtonTapped))
        pencilBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose,
                                              target: self,
                                              action: #selector(didPencilBarButtonTapped))
        navigationItem.rightBarButtonItems = [trashBarButtonItem, pencilBarButtonItem]
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }

    @objc private func didTrashBarButtonTapped(_ sender: UIBarButtonItem) {
        isDeleteMode.toggle()
        updateTrashBarButtonItem()
    }

    @objc private func didPencilBarButtonTapped(_ sender: UIBarButtonItem) {
        isDeleteMode = false
        updateTrashBarButtonItem()
        transitionToEditScreen()
    }

    private func updateTrashBarButtonItem() {
        let systemItem: UIBarButtonItem.SystemItem = isDeleteMode ? .stop : .trash
        let trashBarButtonItem = UIBarButtonItem(barButtonSystemItem: systemItem,
                                                 target: self,
                                                 action: #selector(didTrashBarButtonTapped(_:)))
        navigationItem.rightBarButtonItems = [trashBarButtonItem, pencilBarButtonItem]
    }

    private func transitionToEditScreen(todo: Todo? = nil) {
        let storyboard = UIStoryboard(name: "Edit", bundle: nil)
        guard let todoEditViewController = storyboard.instantiateInitialViewController() as? TodoEditViewController else { return }
        todoEditViewController.delegate = self
        todoEditViewController.todo = todo
        navigationController?.pushViewController(todoEditViewController, animated: true)
    }

    private func showSnackbar(text: String) {
        let snackbar = MDCSnackbarMessage()
        snackbar.text = text
        MDCSnackbarManager.show(snackbar)
    }

    private func showDeleteAlert(todo: Todo) {
        let alertController = UIAlertController(title: "「\(todo.title)」を削除します。", message: "よろしいですか?", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "キャンセル", style: .cancel))
        let deleteAction = UIAlertAction(title: "OK", style: .default) { _ in
            self.deleteTodo(id: todo.id)
        }
        alertController.addAction(deleteAction)
        present(alertController, animated: true)
    }
}

extension TodoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        todos.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath)
        cell.textLabel?.text = todos[indexPath.row].title
        return cell
    }
}

extension TodoListViewController: TodoEditViewControllerDelegate {
    func todoEditViewControllerDidEditTodo(_ todoEditViewController: TodoEditViewController, message: String) {
        navigationController?.popViewController(animated: true)
        showSnackbar(text: message)
        fetchTodoList()
    }
}

extension TodoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let todo = todos[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        if isDeleteMode {
            showDeleteAlert(todo: todo)
        } else {
            transitionToEditScreen(todo: todo)
        }
    }
}
