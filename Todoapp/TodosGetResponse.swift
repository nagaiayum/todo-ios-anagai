//
//  TodosGetResponse.swift
//  Todoapp
//
//  Created by anagai on 2020/06/25.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import Foundation

struct TodosGetResponse: BaseResponse {
    var todos: [Todo]
    var errorCode: Int
    var errorMessage: String
}
