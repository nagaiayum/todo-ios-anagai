//
//  UIViewController+.swift
//  Todoapp
//
//  Created by anagai on 2020/07/06.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import UIKit

extension UIViewController {
    func showErrorAlert(message: String) {
        let alertController = UIAlertController(title: "エラー",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))
        present(alertController, animated: true)
    }
}
